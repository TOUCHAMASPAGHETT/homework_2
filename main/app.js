function addTokens(input, tokens){
    
    //checking if input is valid
    if (typeof input !== "string")
        throw new Error('Invalid input');

    if (input.length < 6)
        throw new Error('Input should have at least 6 characters');

    //checking if tokens are valid - didn't really know how to validate aside for
    // 1. checking if tokens is an array
    // 2. checking if the elements all have the tokenName field
    // 3. and checking if the values are strings
    if (!Array.isArray(tokens))
        throw new Error('Invalid array format');

    for (let token of tokens) {
        if (!token.tokenName || typeof token.tokenName !== "string")
            throw new Error('Invalid array format');
    }

    let index = input.indexOf('...');
    if (index === -1)
        return input;
    let newString = input;

    tokens.map(token => {
        index = newString.indexOf('...');
        newString = newString.slice(0, index) + '${' + token.tokenName + '}' + newString.slice(index+3);
    })

    return newString;
}

const app = {
    addTokens: addTokens
}

module.exports = app;